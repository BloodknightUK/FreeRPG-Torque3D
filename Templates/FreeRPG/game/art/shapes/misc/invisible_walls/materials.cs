

singleton Material(invisible_wall_10x10m_invisible_wall_01)
{
   mapTo = "invisible_wall_01";
   diffuseColor[0] = "0.64 0.64 0.64 1";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};
