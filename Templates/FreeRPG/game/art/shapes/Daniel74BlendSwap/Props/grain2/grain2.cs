
singleton TSShapeConstructor(Grain2Dae)
{
   baseShape = "./grain2.dae";
   adjustCenter = "1";
   adjustFloor = "1";
   loadLights = "0";
};
