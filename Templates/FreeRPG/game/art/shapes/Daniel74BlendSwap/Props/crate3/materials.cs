
singleton Material(crate3_Cabbage)
{
   mapTo = "Cabbage";
   diffuseMap[0] = "untitled";
   specular[0] = "0.01 0.01 0.01 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};
