
singleton Material(OldOfficeDesk_01_OldDeskHandles)
{
   mapTo = "OldDeskHandles";
   diffuseMap[0] = "3TD_OldDeskHandle_01";
   specular[0] = "0.996078 0.996078 0.996078 1";
   specularPower[0] = "128";
   translucentBlendOp = "None";
   specularStrength[0] = "5";
   pixelSpecular[0] = "1";
   useAnisotropic[0] = "1";
   doubleSided = "1";
};

singleton Material(OldOfficeDesk_01_OldDeskTop)
{
   mapTo = "OldDeskTop";
   diffuseMap[0] = "3td_Formica_01_White";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
   pixelSpecular[0] = "1";
   useAnisotropic[0] = "1";
   doubleSided = "1";
   specularStrength[0] = "0.2";
};

singleton Material(OldOfficeDesk_01_OldDeskGrayMetal)
{
   mapTo = "OldDeskGrayMetal";
   diffuseMap[0] = "3TD_OldOfficeDesk_Gray";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "38";
   translucentBlendOp = "None";
   specularStrength[0] = "3.82353";
   pixelSpecular[0] = "1";
   useAnisotropic[0] = "1";
   doubleSided = "1";
};
