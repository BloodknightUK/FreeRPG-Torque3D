
singleton Material(OldWagon_Rusty01_jpg)
{
   mapTo = "Rusty01_jpg";
   diffuseMap[0] = "3td_Rust_01";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "25";
   translucentBlendOp = "None";
   normalMap[0] = "3td_Rust_01_NRM.png";
   useAnisotropic[0] = "1";
   doubleSided = "1";
};

singleton Material(OldWagon_DecayWood01_jpg)
{
   mapTo = "DecayWood01_jpg";
   diffuseMap[0] = "3td_OldWood_01";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "25";
   translucentBlendOp = "None";
   useAnisotropic[0] = "1";
   doubleSided = "1";
};

singleton Material(OldWagon_Metal_19_jpg)
{
   mapTo = "Metal_19_jpg";
   diffuseMap[0] = "3td_metal_19";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "25";
   translucentBlendOp = "None";
   useAnisotropic[0] = "1";
   doubleSided = "1";
};

singleton Material(OldWagon_Wood_23_jpg)
{
   mapTo = "Wood_23_jpg";
   diffuseMap[0] = "3td_Planks_02";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "25";
   translucentBlendOp = "None";
   normalMap[0] = "3td_Planks_02_NRM.png";
   useAnisotropic[0] = "1";
   doubleSided = "1";
};
