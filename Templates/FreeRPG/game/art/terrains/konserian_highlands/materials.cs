new TerrainMaterial()
{
   diffuseMap = "art/terrains/konserian_highlands/soil_gravel_base";
   diffuseSize = "200";
   normalMap = "art/terrains/konserian_highlands/soil_gravel_normal";
   detailMap = "art/terrains/konserian_highlands/soil_gravel_detail";
   detailDistance = "150";
   parallaxScale = "0.07";
   internalName = "soil_gravel";
};

new TerrainMaterial()
{
   diffuseMap = "art/terrains/konserian_highlands/moss_gravel_sparse_base";
   diffuseSize = "200";
   normalMap = "art/terrains/konserian_highlands/moss_gravel_sparse_normal";
   detailMap = "art/terrains/konserian_highlands/moss_gravel_sparse_detail";
   detailSize = "10";
   detailDistance = "150";
   parallaxScale = "0.05";
   internalName = "moss_gravel_sparse";
};

new TerrainMaterial()
{
   diffuseMap = "art/terrains/konserian_highlands/moss_gravel_dense_base";
   diffuseSize = "250";
   normalMap = "art/terrains/konserian_highlands/moss_gravel_dense_normal";
   detailMap = "art/terrains/konserian_highlands/moss_gravel_dense_detail";
   detailSize = "10";
   detailDistance = "150";
   parallaxScale = "0.05";
   internalName = "moss_gravel_dense";
};

new TerrainMaterial()
{
   diffuseMap = "art/terrains/konserian_highlands/moss_gravel_medium_base";
   diffuseSize = "250";
   normalMap = "art/terrains/konserian_highlands/moss_gravel_medium_normal";
   detailMap = "art/terrains/konserian_highlands/moss_gravel_medium_detail";
   detailSize = "10";
   detailDistance = "150";
   parallaxScale = "0.05";
   internalName = "moss_gravel_medium";
};

new TerrainMaterial()
{
   diffuseMap = "art/terrains/konserian_highlands/rock_rough_mossy_base";
   diffuseSize = "500";
   normalMap = "art/terrains/konserian_highlands/rock_rough_mossy_normal";
   detailMap = "art/terrains/konserian_highlands/rock_rough_mossy_detail";
   detailSize = "10";
   detailDistance = "150";
   parallaxScale = "0.08";
   internalName = "rock_rough_mossy";
   detailStrength = "1";
};